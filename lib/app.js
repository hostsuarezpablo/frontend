var $ = window.jQuery

var key = 'c50519606f6ab8e3ccf48078705adcae'
var api = new MarvelApi(key)

api.findSeries('avengers')
.then((serie) => {
	let serieImage = `url(${serie.thumbnail.path}.${serie.thumbnail.extension})`
	$('.Layout').css('background-image', serieImage)
	// luego de resolver la llamada cargo en un array
	// todos los characters, una ves que termine devuelvo
	// todos los personajes
	var characters = serie.characters.items
	var promises = []
	for (let character of characters) {
		let promise = api.getResourceURI(character.resourceURI)
		// mi function de la api ya me devuelve la promise
		promises.push(promise)
	}
	//	Promise.all se resuelve cuando todas las promesas del array se resuelvan
	return Promise.all(promises)
})
.then((characters) => {
	return characters.filter((character) => {
		return !!character.thumbnail && !!character.name && !!character.description
	})
})
// Cuando finalice promises que es un array de Promises
// se ejecuta este then
.then((characters) => {
	characters = shuffle(characters)
	// console.log(characters)

	for (let i = 0; i < 5; i++) {
		let character = characters[i]
		// console.log(character)
		// console.log("name:" + character.name + " description:" + character.description + " thumbnail:" + character.thumbnail.path + character.thumbnail.extension)
		let $card = $(renderCharacter(character))
		$('.Battle-player').append($card)
	}
})
.catch((err) => {
	console.error(err)
})

function renderCharacter(character){
	let attackPoint = Math.ceil(Math.random() * 500 + 500)
	// genero numero deñ 500  al 1000
	return `
		<div class="Card">
	      <h2 class="Card-name">${character.name}</h2><img src="${character.thumbnail.path}.${character.thumbnail.extension}" alt="${character.name}" class="Card-image">
	      <div class="Card-description">${character.description}</div>
	      <div class="Card-attack">${attackPoint} puntos de ataque</div>
	    </div>`
}

function shuffle(arr){
	for (let i = 0; i < arr.length; i++) {
		let tmp = arr[i]
		let index = Math.floor(Math.random() * arr.length)
		arr[i] = arr[index]
		arr[index] = tmp
	}
	return arr
}
