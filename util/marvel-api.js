var $ = window.jQuery;

// Defino clases en js
class MarvelApi {
	constructor(key) {
		this.key = key
		this.baseUrl = 'http://gateway.marvel.com/v1/public/';
	}

	findSeries(title) {
		// let nueva forma de definir variables
		let url = `${this.baseUrl}series?title=${title}&apikey=${this.key}`;
		// return Promise.resolve($.get(url))
		return api.getPromiseCache(url)
	}

	getResourceURI(resourceURI) {
		let url = `${resourceURI}?apikey=${this.key}`;

		// return Promise.resolve($.get(url))
		return api.getPromiseCache(url)
	}

	getPromiseCache(url) {
		if(localStorage[url]){
			let datos = JSON.parse(localStorage[url]).data.results[0]
			// console.log('Cargo desde Cache url:'+url+" // "+localStorage[url])
			return Promise.resolve(datos)
		}
		return Promise.resolve($.get(url))
		.then((data) => {
			let datos = data
			// paso el objeto a string
			datos = JSON.stringify(datos)
			localStorage[url] = datos
			return Promise.resolve(data)
			.then((res) => {
				return res.data.results[0]
			})
		})
	}

}

//Para llamar la clase es new MarvelApi(key)
window.MarvelApi = MarvelApi
