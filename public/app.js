'use strict';

var $ = window.jQuery;

var key = 'c50519606f6ab8e3ccf48078705adcae';
var api = new MarvelApi(key);

api.findSeries('avengers').then(function (serie) {
	var serieImage = 'url(' + serie.thumbnail.path + '.' + serie.thumbnail.extension + ')';
	$('.Layout').css('background-image', serieImage);
	// luego de resolver la llamada cargo en un array
	// todos los characters, una ves que termine devuelvo
	// todos los personajes
	var characters = serie.characters.items;
	var promises = [];
	var _iteratorNormalCompletion = true;
	var _didIteratorError = false;
	var _iteratorError = undefined;

	try {
		for (var _iterator = characters[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
			var character = _step.value;

			var promise = api.getResourceURI(character.resourceURI);
			// mi function de la api ya me devuelve la promise
			promises.push(promise);
		}
	} catch (err) {
		_didIteratorError = true;
		_iteratorError = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion && _iterator['return']) {
				_iterator['return']();
			}
		} finally {
			if (_didIteratorError) {
				throw _iteratorError;
			}
		}
	}

	//	Promise.all se resuelve cuando todas las promesas del array se resuelvan
	return Promise.all(promises);
}).then(function (characters) {
	return characters.filter(function (character) {
		return !!character.thumbnail && !!character.name && !!character.description;
	});
})
// Cuando finalice promises que es un array de Promises
// se ejecuta este then
.then(function (characters) {
	characters = shuffle(characters);
	// console.log(characters)

	for (var i = 0; i < 5; i++) {
		var character = characters[i];
		// console.log(character)
		// console.log("name:" + character.name + " description:" + character.description + " thumbnail:" + character.thumbnail.path + character.thumbnail.extension)
		var $card = $(renderCharacter(character));
		$('.Battle-player').append($card);
	}
})['catch'](function (err) {
	console.error(err);
});

function renderCharacter(character) {
	var attackPoint = Math.ceil(Math.random() * 500 + 500);
	// genero numero deñ 500  al 1000
	return '\n\t\t<div class="Card">\n\t      <h2 class="Card-name">' + character.name + '</h2><img src="' + character.thumbnail.path + '.' + character.thumbnail.extension + '" alt="' + character.name + '" class="Card-image">\n\t      <div class="Card-description">' + character.description + '</div>\n\t      <div class="Card-attack">' + attackPoint + ' puntos de ataque</div>\n\t    </div>';
}

function shuffle(arr) {
	for (var i = 0; i < arr.length; i++) {
		var tmp = arr[i];
		var index = Math.floor(Math.random() * arr.length);
		arr[i] = arr[index];
		arr[index] = tmp;
	}
	return arr;
}
