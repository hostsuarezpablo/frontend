'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var $ = window.jQuery;

// Defino clases en js

var MarvelApi = (function () {
	function MarvelApi(key) {
		_classCallCheck(this, MarvelApi);

		this.key = key;
		this.baseUrl = 'http://gateway.marvel.com/v1/public/';
	}

	_createClass(MarvelApi, [{
		key: 'findSeries',
		value: function findSeries(title) {
			// let nueva forma de definir variables
			var url = this.baseUrl + 'series?title=' + title + '&apikey=' + this.key;
			// return Promise.resolve($.get(url))
			return api.getPromiseCache(url);
		}
	}, {
		key: 'getResourceURI',
		value: function getResourceURI(resourceURI) {
			var url = resourceURI + '?apikey=' + this.key;

			// return Promise.resolve($.get(url))
			return api.getPromiseCache(url);
		}
	}, {
		key: 'getPromiseCache',
		value: function getPromiseCache(url) {
			if (localStorage[url]) {
				var datos = JSON.parse(localStorage[url]).data.results[0];
				// console.log('Cargo desde Cache url:'+url+" // "+localStorage[url])
				return Promise.resolve(datos);
			}
			return Promise.resolve($.get(url)).then(function (data) {
				var datos = data;
				// paso el objeto a string
				datos = JSON.stringify(datos);
				localStorage[url] = datos;
				return Promise.resolve(data).then(function (res) {
					return res.data.results[0];
				});
			});
		}
	}]);

	return MarvelApi;
})();

//Para llamar la clase es new MarvelApi(key)
window.MarvelApi = MarvelApi;
